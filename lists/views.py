from django.core.exceptions import ValidationError
from django.shortcuts import render, redirect
from lists.forms import ItemForm, ExistingListItemForm
from lists.models import Item, List


def home_page(request):
    return render(request, 'home.html', {'form': ItemForm()})


def new_list(request):
    form = ItemForm(data=request.POST)
    if form.is_valid():
        list_one = List.objects.create()
        Item.objects.create(text=request.POST['text'], list=list_one)
        return redirect(list_one)
    else:
        return render(request, 'home.html', {"form": form})


def view_list(request, list_id):
    list_one = List.objects.get(id=list_id)
    form = ExistingListItemForm(for_list=list_one)
    if request.method == 'POST':
        form = ExistingListItemForm(for_list=list_one, data=request.POST)
        if form.is_valid():
            form.save()
            return redirect(list_one)
    return render(request, 'list.html', {'list': list_one, "form": form})




